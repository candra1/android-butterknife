package co.g2academy.android_butterknife.networking;

import androidx.lifecycle.MutableLiveData;

import co.g2academy.android_butterknife.model.NasabahsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NasabahsRepository {
    private static NasabahsRepository nasabahsRepository;
    private NasabahApi nasabahApi;

    public static NasabahsRepository getInstance(){
        if (nasabahsRepository == null){
            nasabahsRepository = new NasabahsRepository();
        }
        return nasabahsRepository;
    }

    public NasabahsRepository(){
        nasabahApi = RetrofitService.cteateService(NasabahApi.class);
    }

    public MutableLiveData<NasabahsResponse> getNasabahs(String page, String limit){
        MutableLiveData<NasabahsResponse> nasabahsData = new MutableLiveData<>();
        nasabahApi.getNasabahsList(page, limit).enqueue(new Callback<NasabahsResponse>() {
            @Override
            public void onResponse(Call<NasabahsResponse> call,
                                   Response<NasabahsResponse> response) {
                if (response.isSuccessful()){
                    nasabahsData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<NasabahsResponse> call, Throwable t) {
                nasabahsData.setValue(null);
            }
        });
        return nasabahsData;
    }
}
