package co.g2academy.android_butterknife.networking;

import co.g2academy.android_butterknife.model.NasabahsResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NasabahApi {
    @GET("nasabah")
    Call<NasabahsResponse> getNasabahsList(@Query("page") String page,
                                           @Query("limit") String limit);



}
