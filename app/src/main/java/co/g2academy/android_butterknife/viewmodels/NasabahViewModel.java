package co.g2academy.android_butterknife.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import co.g2academy.android_butterknife.model.NasabahsResponse;
import co.g2academy.android_butterknife.networking.NasabahsRepository;


public class NasabahViewModel extends ViewModel {
    private MutableLiveData<NasabahsResponse> mutableLiveData;
    private NasabahsRepository nasabahsRepository;

    public void init(){
        if (mutableLiveData != null){
            return;
        }
        nasabahsRepository = NasabahsRepository.getInstance();
        mutableLiveData = nasabahsRepository.getNasabahs("1", "10");
    }

    public LiveData<NasabahsResponse> getNasabahsRepository() {
        return mutableLiveData;
    }
    public void refresh(String page, String limit ){
        if (mutableLiveData != null){
            mutableLiveData = nasabahsRepository.getNasabahs(page, limit);
            return;
        }
        nasabahsRepository = NasabahsRepository.getInstance();
        mutableLiveData = nasabahsRepository.getNasabahs("1", "10");
    }
}
