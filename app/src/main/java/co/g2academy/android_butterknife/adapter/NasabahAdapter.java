package co.g2academy.android_butterknife.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.g2academy.android_butterknife.model.Nasabah;
import co.g2academy.android_butterknife.R;

public class NasabahAdapter extends RecyclerView.Adapter<NasabahAdapter.NasabahViewHolder> {

    Context context;
    ArrayList<Nasabah> nasabahs;

    public NasabahAdapter(Context context, ArrayList<Nasabah> nasabahs) {
        this.context = context;
        this.nasabahs = nasabahs;
    }

    @NonNull
    @Override
    public NasabahViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_nasabah, parent, false);
        return new  NasabahViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NasabahViewHolder holder, int position) {
        holder.namaTv.setText(nasabahs.get(position).getNama());
        holder.alamatTv.setText(nasabahs.get(position).getAlamat());
        holder.emailTv.setText(nasabahs.get(position).getEmail());

        holder.nasabahLl.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),nasabahs.get(position).getNama(),Toast.LENGTH_SHORT).show();
            };
        });
//        Picasso.get().load(nasabahs.get(position).getUrlToImage()).into(holder.nasabahIv);
    }

    @Override
    public int getItemCount() {
        return nasabahs.size();
    }

    public class NasabahViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.namaTextView) TextView namaTv;
        @BindView(R.id.alamatTextView) TextView alamatTv;
        @BindView(R.id.emailTextView) TextView emailTv;
        @BindView(R.id.nasabahImageView) ImageView nasabahIv;
        @BindView(R.id.nasabahLl) LinearLayout nasabahLl;

        public NasabahViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }



}
