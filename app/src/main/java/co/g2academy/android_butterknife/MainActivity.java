package co.g2academy.android_butterknife;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.OnClick;
import butterknife.OnItemSelected;
import co.g2academy.android_butterknife.adapter.NasabahAdapter;
import co.g2academy.android_butterknife.model.Nasabah;
import co.g2academy.android_butterknife.viewmodels.NasabahViewModel;
import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    ArrayList<Nasabah> nasabahArrayList = new ArrayList<>();
    NasabahAdapter nasabahAdapter;

    @BindView(R.id.nasabahRecyclerView)
    RecyclerView rvNasabah;

    NasabahViewModel nasabahViewModel;

    @BindView(R.id.refreshTextView)
    TextView refreshTextView;

    List<Nasabah> nasabahs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initData();
    }

    private void initData() {
        if (nasabahAdapter == null) {
            nasabahAdapter = new NasabahAdapter(MainActivity.this, nasabahArrayList);
            rvNasabah.setLayoutManager(new LinearLayoutManager(this));
            rvNasabah.setAdapter(nasabahAdapter);
            rvNasabah.setItemAnimator(new DefaultItemAnimator());
            rvNasabah.setNestedScrollingEnabled(true);
        } else {
            nasabahAdapter.notifyDataSetChanged();
        }
        nasabahViewModel = ViewModelProviders.of(this).get(NasabahViewModel.class);

        nasabahViewModel.init();
        nasabahViewModel.getNasabahsRepository().observe(this, nasabahsResponse -> {
            nasabahs = nasabahsResponse.getData();
            nasabahArrayList.clear();
            nasabahArrayList.addAll(nasabahs);
            nasabahAdapter.notifyDataSetChanged();
        });


    }
    private void getListNasabah(String page, String limit ){
        nasabahViewModel.refresh(page,limit);
        nasabahViewModel.getNasabahsRepository().observe(this, nasabahsResponse -> {
            nasabahs = nasabahsResponse.getData();
            nasabahArrayList.clear();
            nasabahArrayList.addAll(nasabahs);
            nasabahAdapter.notifyDataSetChanged();
        });
    }

    @OnClick(R.id.refreshTextView)
    public void submit(View view) {
        getListNasabah("1","20");
    }

    @Override
    protected void onResume() {
        super.onResume();
        getListNasabah("1","20");
    }

}